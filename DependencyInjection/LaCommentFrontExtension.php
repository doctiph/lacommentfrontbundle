<?php

namespace La\CommentFrontBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class LaCommentFrontExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('la_comment_front.allow_anonymous', $config['allow_anonymous']);
        $container->setParameter('la_comment_front.a_priori_moderation', $config['a_priori_moderation']);
        $container->setParameter('la_comment_front.captcha_validation', $config['captcha_validation']);
        $container->setParameter('la_comment_front.allow_vote', $config['allow_vote']);
        $container->setParameter('la_comment_front.allow_note', $config['allow_note']);
        $container->setParameter('la_comment_front.post_comment_authentication', $config['post_comment_authentication']);
        $container->setParameter('la_comment_front.comments_resource_url', $config['comments_resource_url']);
        $container->setParameter('la_comment_front.client_id', $config['client_id']);
        $container->setParameter('la_comment_front.client_pw', $config['client_pw']);
        $container->setParameter('la_comment_front.login_url', $config['login_url']);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $ymlLoader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $ymlLoader->load('la_comment_front.yml');

    }
}
