<?php

namespace La\CommentFrontBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('la_comment_front');

        $rootNode->
            children()
                ->booleanNode('allow_anonymous')
                    ->defaultTrue()
                ->end()
                ->booleanNode('a_priori_moderation')
                    ->defaultFalse()
                ->end()
                ->booleanNode('captcha_validation')
                    ->defaultFalse()
                ->end()
                ->booleanNode('allow_vote')
                    ->defaultFalse()
                ->end()
                ->booleanNode('allow_note')
                    ->defaultFalse()
                ->end()
                ->booleanNode('post_comment_authentication')
                    ->defaultFalse()
                ->end()

                ->scalarNode('comments_resource_url')
                    ->isRequired()
                ->end()
                ->scalarNode('login_url')
                    ->isRequired()
                ->end()
                ->scalarNode('client_id')
                    ->isRequired()
                ->end()
                ->scalarNode('client_pw')
                    ->isRequired()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
