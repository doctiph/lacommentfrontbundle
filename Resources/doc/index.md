LaCommentFrontBundle
============

# Prerequis

Symfony 2.3

# Installation

### Nommage
Pour la documentation nous utiliserons le site www.elle.fr avec l'identification sur le domaine identification.elle.fr

# RegisterBundle
Enregistrer les bundles dans app/AppKernel.php si ce n'est pas déjà fait.

    public function registerBundles()
    {
        $bundles = array(
            // ....
            new La\CommentFrontBundle\LaCommentFrontBundle(),
            // ....
        );
    }

# Import des fichiers de routing

### app/config/routing.yml

    la_comment:
        resource: "@LaCommentFrontBundle/Resources/config/routing/routing.yml"
        prefix:   /comments
        defaults: { _format: json }


    la_comment_front_demo:
        resource: "@LaCommentFrontBundle/Resources/config/routing/routing_dev.yml"
        prefix:   /

# Configuration


## Paramétrage du CommentFrontBundle :

### app/config/config.yml :

    la_comment_front:
          comments_resource_url: http://identification.elle.fr/api/threads  # URL de l'API REST de  commentaires.
          login_url: /app_dev.php/login                                     # URL de login vers laquelle rediriger lorsqu'un poster n'est pas authentifié
          client_id: web                                                    # Identifiant du client autorisé à interroger l'API
          client_pw: 0315810930da7a9b3554e0edc12dfccf315598b1               # Password du client autorisé à interroger l'API

# Implémentation du Manager ApiComment

Il est nécessaire de créer un manager pour les commentaires dans le bundle de votre application qui hérite de *La\CommentFrontBundle\Model\ApiCommentManager*,
qui est une classe abstraite devant être étendue.
Ce manager permet de relier votre site, et plus particulièrement votre système de gestion des utilisateurs, à l'API Commentaires.

## Création du manager

La classe ci-dessous en est un exemple d'implémentation :

    namespace La\ElleUserBundle\Model;

    use Symfony\Component\HttpFoundation\Request;
    use La\CommentFrontBundle\Model\CommentUser;
    use La\CommentFrontBundle\Model\ApiCommentManager as baseApiCommentManager;

    class ApiCommentManager extends baseApiCommentManager
    {

       /**
        * Define how a thread id will be generated for a given content on your website.
        *
        * @param $contentId
        * @return mixed
        */
        public function threadIdFactory($contentId)
        {
            return md5($contentId);
        }

       /**
        * Define how you get a CommentUser from a HTTP Request.
        *
        * @param \Symfony\Component\HttpFoundation\Request $request
        * @return CommentUser|null
        */
        public function getCommentUser(Request $request)
        {
            $user = $this->securityContext->getToken()->getUser();
            $cookie = $this->getCommentUserCookie($request);

            $commentUser = new CommentUser();
            $commentUser->setCookie($cookie);

            if ($user == 'anon.') {
                $commentUser->setLogged(false);
                $commentUser->setUsername('Anonyme');
                $commentUser->setEmail(null);
                $commentUser->setAvatar(null);
                $commentUser->setUserId(null);
            } else {
                $commentUser->setLogged(true);
                $commentUser->setUsername($user->getUsername());
                $commentUser->setEmail($user->getEmail());
                $commentUser->setAvatar(null);
                $commentUser->setUserId($this->getCommentUserIdFromUser($user));
            }

            return $commentUser;
        }

        /**
         * Define how you get the CommentUserId from a user.
         *
         */
        public function getCommentUserIdFromUser($user){
            return intval($user->getId());
        }

        /**
         * Define how you get a user from a Comment User
         *
         * @param CommentUser $commentUser
         * @return User
         */
        public function getUserFromCommentUser(CommentUser $commentUser)
        {
            return new User($commentUser->getUsername(), '');
        }

## Ajout de la classe du manager en paramètre dans les services

Dans la définition de vos services (exemple: services.yml), surcharger le paramètre la_comment.front.comment_manager.class.
avec le nom de la classe nouvellement créée :

           <parameters>
               <parameter key="la_comment.front.comment_manager.class">La\ElleUserBundle\Model\ApiCommentManager</parameter>
           </parameters>


# Création des assets

## Créer un lien symbolique des assets (css, js, images...) dans le dossier web/

    php app/console assets:install --symlink --relative

# Démo

Route de démo :

    profile.tld.fr/app_dev.php/demo/comments/1

La route de démo n'est accessible qu'en environnemetn de développement (d'où app_dev.php).
L'id '1' au bout de la route représente le contenu commentable 1. Les contenus existent dans la démo de 1 à 5, mais vous
 êtes libre de tester avec n'importe quel ID, notamment pour vérifier la génération de thread ids dans votre base de donnée.