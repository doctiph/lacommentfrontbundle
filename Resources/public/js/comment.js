var la_comment_front_bundle;

la_comment_front_bundle = (function () {
    "use strict";
    /*global document, window, console, XMLHttpRequest, JSON */
    /*jslint todo: true */

    return {
        /**
         * Token CSRF
         */
        token_identifier: '_token',
        /**
         * Html selectors
         */
        selectors: {
            thread_stats_container: '#la_comment_front_bundle_stats',
            comment_action_container: '#la_comment_front_bundle_comment_actions_%s',
            comment_reply: 'a.la_comment_front_bundle_comment_reply',
            submit_comment: '#la_front_comment_post_send',
            submit_comment_parent_id: '#la_front_comment_post_%s_send',
            sorter_link: '.la_comment_front_bundle_sorter',
            comment_vote: '.la_comment_front_bundle_comment_vote',
            comment_reply_error: '#la_front_bundle_error_message',
            comment_reply_error_parent_id: '#la_front_bundle_error_message_%s',
            comment_content_container: '#la_comment_front_bundle_display',
            comment_form: '#la_front_comment_post',
            comment_form_parent_id: '#la_front_comment_post_%s',
            comment_comment_container: '#la_comment_front_bundle_comment_display_%s',
            marked_for_deletion: '.la_front_to_delete'
        },

        /**
         * Prefixes
         */
        prefixes: {
            comment_reply: 'la_comment_front_bundle_comment_reply_display_',
            comment_edit: 'la_comment_front_bundle_comment_edit_',
            comment_delete: 'la_comment_front_bundle_comment_delete_',
            sorter_link: 'la_comment_front_bundle_sorter_',
            comment_hash: 'la_comment_',
            comment_global_vote: 'la_comment_front_bundle_comment_vote_',
            comment_vote: 'comment_vote_',
            comment_post_comment: 'la_front_comment_post_comment',
            comment_form_name: 'la_front_comment_post',
            comment_form_name_parent_id: 'la_front_comment_post_%s',
            marked_for_deletion: 'la_front_to_delete'
        },

        /**
         * Delay between 2 clicks
         */
        default_delay: 5000,

        /**
         * Thread Id
         */
        thread_id: null,

        /**
         * Thread User Info
         */
        thread_user_info: null,

        /**
         * User Info
         */
        user_info: null,

        /**
         * Timeout
         */
        to: null,

        /**
         * get sorted thread content
         *
         * @param sorter
         */
        display: function (sorter) {
            var
                client,
                sorter_url,
                $$ = this;

            sorter_url = this.thread_ajax_url();
            if (sorter) {
                sorter_url = sorter_url.concat('/', sorter);
            }
            client = new XMLHttpRequest();
            client.open('GET', sorter_url, true);
            client.send();

            client.onreadystatechange = function () {
                if (client.readyState === 4 && client.status === 200) {
                    document.querySelector($$.selectors.comment_content_container).innerHTML = client.responseText;
                    $$.bindAll();
                    $$.onDisplaySuccess();
                    $$.onDOMChange();
                }
            };
        },

        /**
         * Post a comment
         *
         * @param parentId
         * @returns {boolean}
         */
        send: function (parentId) {
            var
                i,
            // form
                form, action,
            // form inputs
                comment, token, errorElement, title, rating,
            // Request
                client, data, dataWithToken, activeAction,
            // this
                formName,
                $$ = this, cleanup;

            if (undefined === parentId) {
                parentId = null;
            }

            if (parentId) {
                errorElement = document.querySelector(this.selectors.comment_reply_error_parent_id.replace('%s', parentId));
                form = document.querySelector(this.selectors.comment_form_parent_id.replace('%s', parentId));
                formName = this.prefixes.comment_form_name_parent_id.replace('%s', parentId);
            } else {
                errorElement = document.querySelector(this.selectors.comment_reply_error);
                form = document.querySelector(this.selectors.comment_form);
                formName = this.prefixes.comment_form_name;
                this.deleteReplyForms();
            }

            action = form.getAttribute('action');

            // Comment
            comment = encodeURIComponent(document.forms[formName][formName.concat('[comment]')].value);
//            resourceId = document.forms[formName][formName.concat('[resourceId]')].value;
            token = encodeURIComponent(document.forms[formName][formName.concat('[', this.token_identifier, ']')].value);

            data = ''.concat(formName, '[comment]=', comment, '&', formName, '[threadId]=', this.thread_id);

            // Title
            title = document.forms[formName][formName.concat('[title]')];
            if (undefined !== title) {
                data = data.concat('&', formName, '[title]=', encodeURIComponent(title.value));
            }

            // Rating
            rating = document.forms[formName][formName.concat('[rating]')];


            if (undefined !== rating) {
                for (i = 0; i < rating.length; i += 1) {
                    if (rating[i].checked) {
                        data = data.concat('&', formName, '[rating]=', encodeURIComponent(rating[i].value));
                        break;
                    }
                }
            }
            dataWithToken = ''.concat(data, '&', formName, '[', this.token_identifier, ']=', token);

            //, '&', formName, '[', this.token_identifier, ']=', token
            // On calme les ardeurs des floodeurs
            if (form.getAttribute('action') === '') {
                return false;
            }
            form.setAttribute('action', '');
            activeAction = function () {
                form.setAttribute('action', action);
            };
            window.clearTimeout(this.to);
            this.to = window.setTimeout(activeAction, $$.default_delay);

            // On reset
            if (undefined !== errorElement) {
                errorElement.innerHTML = '';
            }


            client = new XMLHttpRequest();
            client.open('POST', action, true);
            client.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            client.send(dataWithToken);

            // Quand on post, on réinitialise le sorter pour afficher son post...
            client.onreadystatechange = function () {
                var
                    i,
                    returnUrl,
                    encoded,
                    hashed,
                    index,
                    url;

                // error = 200 + message d'erreur
                if (client.readyState === 4 && client.status === 204) {
                    errorElement.innerHTML = client.responseText;
                    $$.onExceptionSend(client.responseText);
                } else if (client.readyState === 4 && client.status === 206) {
                    // invalid form
                    errorElement.innerHTML = client.responseText;
                    $$.onErrorSend(client.responseText);
                } else if (client.readyState === 4 && client.status === 202) {
                    // pending message
                    $$.cleanForm(formName);
                    $$.deleteReplyForms();
                    $$.onPendingSend();

                  // hope you will agree with that code
                } else if (client.readyState === 4 && client.status === 205) {
                    // Redirect
                    encoded = encodeURIComponent(data);
                    if (parentId) {
                        hashed = '#'.concat($$.prefixes.comment_hash, $$.thread_id, '__', parentId, '=', encoded);
                    } else {
                        hashed = '#'.concat($$.prefixes.comment_hash, $$.thread_id, '=', encoded);
                    }

                    returnUrl = window.location.href;
                    index = returnUrl.indexOf('#');

                    if (index > 0) {
                        returnUrl = returnUrl.substring(0, index);
                    }

                    returnUrl = returnUrl.concat(hashed);

                    url = $$.login_redirect(returnUrl);

                    $$.onRedirectSend(url, client.responseText);

                } else if (client.readyState === 4 && client.status === 200) {
                    $$.cleanForm(formName);
                    $$.onCommentSuccess(client.responseText);
                }
            };
            return false;
        },
        /**
         * Clean forms
         *
         * @param formName
         */
        cleanForm: function (formName) {
            var i, rating;

            if (undefined !== document.forms[formName][formName.concat('[title]')]) {
                document.forms[formName][formName.concat('[title]')].value = '';
            }
            if (undefined !== document.forms[formName][formName.concat('[comment]')]) {
                document.forms[formName][formName.concat('[comment]')].value = '';
            }
            if (undefined !== document.forms[formName][formName.concat('[rating]')]) {
                rating = document.forms[formName][formName.concat('[rating]')];
                for (i = 0; i < rating.length; i += 1) {
                    rating[i].checked = false;
                }
            }
        },


        /**
         * Handler on post success
         *
         * @param comment
         */
        onCommentSuccess: function (comment) {
            var url, index;

            comment = null;
            url = window.location.href;
            index = url.indexOf('#');

            if (index > 0) {
                window.location.href = url;
            }
            this.onSuccess();
            this.display('date_desc');

        },

        /**
         * render a reply form
         *
         * @param parentId
         * @param render
         */
        renderStats: function () {
            var
                client,
                action,
                $$ = this;

            action = this.render_stats_url();

            client = new XMLHttpRequest();
            client.open('GET', action, true);
            client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            client.send();

            client.onreadystatechange = function () {
                if (client.readyState === 4 && client.status === 200) {
                    document.querySelector($$.selectors.thread_stats_container).innerHTML = client.responseText;
                    $$.onDOMChange();
                    return false;
                }
                return false;
            };
        },

        /**
         * render a reply form
         *
         * @param parentId
         * @param render
         */
        renderReply: function (parentId, render, callback) {
            var
                client,
                action,
                $$ = this;

            action = this.render_form_url(parentId);

            client = new XMLHttpRequest();
            client.open('GET', action, true);
            client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            client.send();

            // Quand on post, on réinitialise le sorter pour afficher son post...
            client.onreadystatechange = function () {
                if (client.readyState === 4 && client.status === 200) {
                    render($$, parentId, client.responseText, callback);
                    $$.bindAll();
                    $$.onDOMChange();
                    return false;
                }
                return false;
            };
        },

        deleteReplyForms: function () {
            var
                i, todelete;

            todelete = document.querySelectorAll(this.selectors.marked_for_deletion);
            for (i = 0; i < todelete.length; i += 1) {
                todelete[i].parentNode.removeChild(todelete[i]);
            }
            this.onDOMChange();
        },
        /**
         * Bind reply links
         */
        bindReply: function () {

            var
                i, f,
                comment, comments,
                $$ = this;

            comments = document.querySelectorAll(this.selectors.comment_reply);

            f = function () {
                var
                    parentId,
                    link = this;

                parentId = this.getAttribute('id').substring($$.prefixes.comment_reply.length);


                $$.renderReply(parentId, $$.renderReplyForm, null);

//                $$.renderReply(parentId, function (data) {
//                    $$.deleteReplyForms();
//
//                    link.insertAdjacentHTML('afterEnd', ''.concat('<div class="', $$.prefixes.marked_for_deletion, '">', data, '</div>'));
//                    $$.bindPostButton(parentId);
//                });
                return false;
            };

            for (i = 0; i < comments.length; i += 1) {
                comment = comments[i];
                comment.onclick = f;
            }
        },

        /**
         * Bind main form comment button
         */
        bindPostButton: function (parentId) {
            var
                button, selector,
                $$ = this;

            if (parentId) {
                selector = this.selectors.submit_comment_parent_id.replace('%s', parentId);
            } else {
                selector = this.selectors.submit_comment;
            }
            button = document.querySelector(selector);
            button.onclick = function () {
                $$.send(parentId);
                return false;
            };
        },

        /**
         * Bind sorter links
         */
        bindSorter: function () {
            var
                i, f,
                sorter, sorters,
                $$ = this;

            sorters = document.querySelectorAll(this.selectors.sorter_link);

            f = function () {
                var sorter;

                sorter = this.getAttribute('id').substring($$.prefixes.sorter_link.length);
                $$.display(sorter);
                return false;
            };
            for (i = 0; i < sorters.length; i += 1) {
                sorter = sorters[i];
                sorter.onclick = f;
            }
        },

        /**
         * Bind voter links
         */
        bindVotes: function () {
            var
                i, f,
                vote, votes,
                $$ = this;

            votes = document.querySelectorAll($$.selectors.comment_vote);

            f = function () {
                var i, j, k,
                    client, data, url,
                    commentId,
                    voteValue;

                commentId = this.parentElement.getAttribute('id').substr($$.prefixes.comment_global_vote.length);
                voteValue = this.getAttribute('class').substring($$.selectors.comment_vote.length + $$.prefixes.comment_vote.length);

                // Check if the logged user has already voted the same thing. if so, exit.
                if ((null === $$.user_info)) {
                    return false;
                }

                for (i in $$.user_info) {
                    if ($$.user_info.hasOwnProperty(i)) {
                        if ($$.user_info[i].hasOwnProperty('has_voted')) {
                            if ($$.user_info[i].has_voted) {
                                for (j in $$.user_info[i].has_voted) {
                                    if ($$.user_info[i].has_voted.hasOwnProperty(j)) {
                                        for (k in $$.user_info[i].has_voted[j]) {
                                            if ($$.user_info[i].has_voted[j].hasOwnProperty(k)) {
                                                if (k === commentId) {
                                                    if (parseInt($$.user_info[i].has_voted[j][k], 10) === parseInt(voteValue, 10)) {
                                                        return false;
                                                    } // else
                                                    $$.user_info[i].has_voted[j][k] = voteValue;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                url = $$.thread_ajax_url().concat('/', commentId, '/', voteValue);

                data = ''.concat('la_comment_vote[value]=', voteValue);

                client = new XMLHttpRequest();
                client.open('POST', url, true);
                client.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                client.send(data);

                client.onreadystatechange = function () {
                    var
                        container;

                    if (client.readyState === 4 && client.status === 200) {
                        container = document.querySelector('#'.concat($$.prefixes.comment_global_vote, commentId));
                        container.innerHTML = client.responseText;
                        $$.bindVotes();

                    }
                };
                return false;
            };

            for (i = 0; i < votes.length; i += 1) {
                vote = votes[i];
                vote.onclick = f;
            }
        },

        /**
         * execute all bind functions
         */
        bindAll: function () {
            this.bindReply();
            this.bindSorter();
            this.bindVotes();
        },

        /**
         * bind edit links to logged user post
         */
        bindUserActions: function () {
            var i, j, f, g,
                container,
                todelete,
                edit_link,
                delete_link,
                edit_class = this.prefixes.comment_edit.substr(0, this.prefixes.comment_edit.length - 1),
                delete_class = this.prefixes.comment_delete.substr(0, this.prefixes.comment_delete.length - 1),
                posts = [],

                $$ = this;

            if ((null === $$.thread_user_info)) {
                return false;
            }

            for (i in $$.user_info) {
                if ($$.user_info.hasOwnProperty(i)) {
                    if ($$.user_info[i].hasOwnProperty('has_posted')) {
                        if ($$.user_info[i].has_posted) {
                            posts = $$.user_info[i].has_posted;
                        }
                    }
                }
            }

            f = function () {
                var
                    commentId;

                commentId = this.getAttribute('id').substr($$.prefixes.comment_edit.length);

//                alert('todo : edit comment #'.concat(commentId));
            };

            g = function () {
                var
                    commentId,
                    client, url,
                    container;

                commentId = this.getAttribute('id').substr($$.prefixes.comment_delete.length);
                container = this.parentElement.parentElement;

                url = $$.thread_ajax_url().concat('/', commentId, '/delete');
                client = new XMLHttpRequest();
                client.open('POST', url, true);
                client.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                client.send();

                client.onreadystatechange = function () {
                    if (client.readyState === 4 && client.status === 200) {
                        container.innerHTML = JSON.parse(client.responseText); //TODO implémenter la lib JSON
                    }
                    return false;
                };
            };

            // Delete edit links to rebind'em
            todelete = document.querySelectorAll('.'.concat(edit_class));
            for (i = 0; i < todelete.length; i += 1) {
                todelete[i].parentNode.removeChild(todelete[i]);
            }
            // delete delete links to rebind'em
            todelete = document.querySelectorAll('.'.concat(delete_class));
            for (i = 0; i < todelete.length; i += 1) {
                todelete[i].parentNode.removeChild(todelete[i]);
            }

            for (i = 0; i < posts.length; i += 1) {
                container = document.querySelector($$.selectors.comment_action_container.replace('%s', posts[i]));

                edit_link = document.createElement("a");
                edit_link.appendChild(document.createTextNode('Editer'));
                edit_link.setAttribute("href", "javascript".concat(":void(0);"));
                edit_link.setAttribute("id", $$.prefixes.comment_edit.concat(posts[i]));
                edit_link.setAttribute("class", edit_class);
                edit_link.onclick = f;
                container.appendChild(edit_link);

                delete_link = document.createElement("a");
                delete_link.appendChild(document.createTextNode('Supprimer'));
                delete_link.setAttribute("href", "javascript".concat(":void(0);"));
                delete_link.setAttribute("id", $$.prefixes.comment_delete.concat(posts[i]));
                delete_link.setAttribute("class", delete_class);
                delete_link.onclick = g;
                container.appendChild(delete_link);

            }
            return false;
        },

        /**
         * Set comment after login
         *
         * @returns {boolean}
         */
        setComment: function () {
            var
                i, j, index,
                encodedData,
                formName,
                parentId,
                keys,
                keysAndComment,
                input,
                f, $$ = this;

            encodedData = window.location.hash.split('#'.concat(this.prefixes.comment_hash));


            if (2 > encodedData.length) {
                return false;
            }
            keysAndComment = encodedData[1].split('=');

            keys = keysAndComment[0].split('=')[0]; // ??
            index = keys.lastIndexOf('__');
            // pas de parents
            if (-1 === index) {
                parentId = null;
            } else {
                // l'id comporte un "_"
                if (0 < parseInt(keys.substr(0, index))) {
                    parentId = null;
                } else {
                    parentId = keys.substr(index + 2);
                }

            }

            f = function () {
                var data,
                    tmp2;

                data = encodedData[1].substr(keys.length + 1);
                data = data.replace(/&#039;/g, '\'');
                data = decodeURIComponent(data);
                data = data.split('&');

                for (i = 0; i < data.length; i += 1) {
                    input = data[i].split('=');

                    if (undefined !== document.forms[formName][input[0]]) {
                        tmp2 = input[0].split('[rating]');
                        if (tmp2.length === 2) { // rating, on coche la bonne case
                            input[1] = input[1] - 1; // le selecteur est égal à value de la case cochée - 1 (eg : si 4 est coché, le selecteur est (...)_3 )
                            document.forms[formName][input[0]][input[1]].checked = true;
                        } else {
                            document.forms[formName][input[0]].value = decodeURIComponent(input[1]);
                        }
                    }

                }
                window.location.href = '#'.concat($$.prefixes.comment_hash, keys);
                $$.send(parentId);
            };

            if (null === parentId) {
                parentId = null;
                formName = this.prefixes.comment_form_name;
                f();

            } else {
                formName = this.prefixes.comment_form_name_parent_id.replace('%s', parentId);
                this.renderReply(parentId, $$.renderReplyForm, f);
            }

            return false;
        },


        renderReplyForm: function ($$, parentId, data, callback) {
            var container;
            container = document.querySelector($$.selectors.comment_action_container.replace('%s', parentId));
            $$.deleteReplyForms();
            container.insertAdjacentHTML('afterEnd', ''.concat('<div class="', $$.prefixes.marked_for_deletion, '">', data, '</div>'));
            $$.bindPostButton(parentId);
            if (callback) {
                callback();
            }
        },


        initUser: function () {
            var
                client,
                $$ = this;

            client = new XMLHttpRequest();
            client.open('GET', $$.thread_ajax_url().concat('/user'), true);
            client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            client.send();

            client.onreadystatechange = function () {
                if (client.readyState === 4 && client.status === 200) {
                    $$.thread_user_info = JSON.parse(client.responseText); //TODO implémenter la lib JSON
                    if ($$.thread_user_info.hasOwnProperty($$.thread_id)) {
                        $$.user_info = $$.thread_user_info[$$.thread_id];
                    }
                    // $$.bindUserActions();

                    return false;
                }
                return false;
            };
        },

        /**
         * Init main script
         *
         * @param thread_id
         */
        init: function (thread_id) {

            this.thread_id = thread_id;
            //this.initUser();
            this.bindPostButton();
            this.bindAll();
            this.setComment();
        },

        // client implementation

        /**
         * login page url
         *
         * @param returnUrl
         * @returns {null}
         */
        login_redirect: function (returnUrl) {
            returnUrl = null;
            return returnUrl;
        },

        /**
         * comentFrontBundle controller route
         *
         * @returns {null}
         */
        thread_ajax_url: function () {
            return null;
        },

        /**
         * Message on a priori moderation
         * @returns {null}
         */
        onPendingSend: function () {
            return null;
        },

        /**
         *
         * @param parentId
         * @returns {null}
         */
        render_form_url: function (parentId) {
            parentId = null;
            return parentId;
        },

        /**
         *
         * @param parentId
         * @returns {null}
         */
        render_stats_url: function () {
            return null;
        },

        /**
         *
         * @returns {null}
         */
        onSuccess: function () {
            return null;
        },

        /**
         *  redirect to the url location
         *
         */
        onRedirectSend: function (url, responseText) {
            window.location.href = url;
        },

        /**
         *
         * @returns {null}
         */
        onErrorSend: function (responseText) {
            return null;
        },

        /**
         *
         * @returns {null}
         */
        onExceptionSend: function (responseText) {
            return null;
        },

        /**
         *
         * @returns {null}
         */
        onDisplaySuccess: function () {
            return null;
        },

        /**
         *
         * @returns {null}
         */
        onDOMChange: function () {
            return null;
        }
    };
}());
