<?php

namespace La\CommentFrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use La\CommentFrontBundle\Model\Thread;

class CommentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Thread $thread */
        $thread = $options['thread'];
        $parentId = $options['parentId'];

        $builder->add('title', 'text', array(
            'label' => 'la_front_comment.label.title',
            'constraints' => array(
                new NotBlank(
                    array('message' => 'la_front_comment.title.blank')
                ),
            )
        ));

        $builder->add('comment', 'textarea', array(
            'label' => 'la_front_comment.label.comment',
            'constraints' => array(
                new NotBlank(
                    array('message' => 'la_front_comment.comment.blank')
                ),
            )
        ));

        // Only review type thread and ancestor-less comment
        if ($thread->getType() === $thread::REVIEW_TYPE && $parentId === null) {
            $builder->add('rating', 'choice', array(
                'label' => 'la_front_comment.label.rating',
                'expanded' => true,
                'choices' => array(
                    1 => 'la_front_comment.rating.one',
                    2 => 'la_front_comment.rating.two',
                    3 => 'la_front_comment.rating.three',
                    4 => 'la_front_comment.rating.four',
                    5 => 'la_front_comment.rating.five'
                ),
                'constraints' => array(
                    new NotBlank(
                        array('message' => 'la_front_comment.rating.blank')
                    ),
                )
            ));
        }

        $builder->add('send', 'button', array(
            'label' => 'la_front_comment.label.send',
        ));

//        $builder->add('resourceId', 'hidden');
        $builder->add('threadId', 'hidden');

    }

    public function getName()
    {
        return 'la_front_comment_post';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
//            'csrf_protection' => false,
            'thread' => null,
            'parentId' => null
        ));
    }

}
