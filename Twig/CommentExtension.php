<?php

namespace La\CommentFrontBundle\Twig;

use La\CommentFrontBundle\Model\Comment;
use La\CommentFrontBundle\Model\Thread;
use La\CommentFrontBundle\Model\CommentManagerInterface;

class CommentExtension extends \Twig_Extension
{
    protected $commentsManager;

    protected $allowedOptions;

    public function __construct(CommentManagerInterface $commentsManager)
    {
        $this->commentsManager = $commentsManager;
        $this->allowedOptions = array(
            'offset' => null,
            'limit'  => null,
        );
    }

    // FUNCTIONS ////////////////////////////////////////////////////////////////////


    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('fetch_comments', array($this, 'fetchComments')),
        );
    }

    public function fetchComments($threadId, $permalink, $sorter, $type, $options = array())
    {
        if ($this->commentsManager->getThread($threadId)->getId() === 'error') {
            if (is_null($type)) {
                $type = Thread::THREAD_TYPE;
            }
            $thread = $this->commentsManager->newThread($threadId, $permalink, $type);
        }

        // get options overloaded by the user
        $overloadedOptions = array_intersect_key($options, $this->allowedOptions);
        $offset = isset($overloadedOptions['offset']) ? $overloadedOptions['offset'] : $this->allowedOptions['offset'];
        $limit = isset($overloadedOptions['limit']) ? $overloadedOptions['limit'] : $this->allowedOptions['limit'];

        return $this->commentsManager->getCommentsFromThread($threadId, $permalink, $sorter, $offset, $limit);
    }


    // FILTER ////////////////////////////////////////////////////////////////////////


    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('is_visible', array($this, 'isVisibleFilter')),
            new \Twig_SimpleFilter('book_cURL', array($this, 'bookCURLFilter', array('is_safe' => array('html')))),
        );
    }

    // @todo delete references
    public function isVisibleFilter(Comment $comment)
    {
        return true;
    }

    public function bookCURLFilter($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($curl);
        curl_close($curl);

        $book = json_decode($return);

        $body = "";
        foreach ($book->p as $i => $p) {
            if ($i == 10) {
                break;
            }
            $body .= "<p>" . $p . "</p>";
        }

        return array(
            "chapter" => "Alice in Wonderland ~ " . $book->ch[0],
            "body" => $body
        );
    }

    public function getName()
    {
        return 'comments_extension';
    }

}
