<?php

namespace La\CommentFrontBundle\Model;

class Thread
{

    const THREAD_TYPE = 0;
    const REVIEW_TYPE = 1;

    protected $id;                       // Thread id
    protected $is_commentable = true;    // Bool : Is thread commentable ?
    protected $is_enabled = true;        // Bool : Is thread enabled ?
    protected $num_comments = 0;         // Number of comments in thread
    protected $last_comment_at = null;   // Datetime of last comment in thread
    protected $permalink;                // Thread permalink
    protected $rating_count = 0;         // Number of rating in comments
    protected $rating_sum = 0;           // Sum of all the rating
    protected $rating = 0;               // Thread global rating ( ratingSum / ratingCount )
    protected $type = self::THREAD_TYPE; // Thread type

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param  string
     * @return null
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * @param  string
     * @return null
     */
    public function setPermalink($permalink)
    {
        $this->permalink = $permalink;
    }

    /**
     * @return bool
     */
    public function isCommentable()
    {
        return $this->is_commentable;
    }

    /**
     * @param  bool
     * @return null
     */
    public function setCommentable($isCommentable)
    {
        $this->is_commentable = (bool) $isCommentable;
    }


    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->is_enabled;
    }

    /**
     * @param  bool
     * @return null
     */
    public function setEnabled($isEnabled)
    {
        $this->is_enabled = (bool) $isEnabled;
    }

    /**
     * Gets the number of comments
     *
     * @return integer
     */
    public function getNumComments()
    {
        return $this->num_comments;
    }

    /**
     * Sets the number of comments
     *
     * @param integer $num_comments
     */
    public function setNumComments($num_comments)
    {
        $this->num_comments = intval($num_comments);
    }

    /**
     * Increments the number of comments by the supplied
     * value.
     *
     * @param  integer $by Value to increment comments by
     * @return integer The new comment total
     */
    public function incrementNumComments($by = 1)
    {
        return $this->num_comments += intval($by);
    }

    /**
     * @return \DateTime
     */
    public function getLastCommentAt()
    {
        return $this->last_comment_at;
    }

    /**
     * @param  DateTime
     * @return null
     */
    public function setLastCommentAt($lastCommentAt)
    {
        $this->last_comment_at = $lastCommentAt;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function addRating($rating)
    {
        $rating = intval($rating);
        if($rating > 5||$rating < 0)
        {
            throw new \Exception('Invalid note for thread '.$this->getId());
        }

        $this->rating_count++;
        $this->rating_sum += $rating;
        $this->rating = round(($this->rating_sum/$this->rating_count),0);
    }

    public function removeRating($rating)
    {
        $this->rating_count--;
        $this->rating_sum -= $rating;
        $this->rating = round(($this->rating_sum/$this->rating_count),0);
    }


    public function __toString()
    {
        return 'Comment thread #'.$this->getId();
    }
}
