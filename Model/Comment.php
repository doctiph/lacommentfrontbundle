<?php

namespace La\CommentFrontBundle\Model;

use La\CommentFrontBundle\Model\CommentUser;

class Comment
{

    /** Cf. CommentBundle */
    const STATE_VISIBLE = 0;
    const STATE_PENDING = 1;
    const STATE_IN_PROGRESS = 2;
    const STATE_VALIDATED = 3;
    const STATE_DELETED = 4;
    const STATE_SPAM = 5;

    protected $id;                          // Comment Id
    protected $title;                       // Title
    protected $parent;                      // Parent Comment Id
    protected $body;                        // Comment body
    protected $depth = 0;                   // Comment depth (# of ancestors)
    protected $created_at;                  // DateTime
    protected $moderationState = 1;         // Moderation state (cf. constants)
    protected $previousModerationState = 1; // Previous moderation state
    protected $visibility = true;           // Current visibility of the comment
    protected $previous_state = 0;          // Previous state (cf. const)
//    protected $thread;                    // Thread Id where the comment is
    protected $commentUser;                 // Comment author
    protected $headers;                     // Serialized Headers (cf. manager)
    protected $upvotes = 0;                 // Upvotes of the comment
    protected $downvotes = 0;               // Downvotes of the comment
    protected $score;                       // Global score of the comment (upvotes - downvotes)
    protected $rating = 0;                  // Thread rating of this comment (cf. Thread)

    public function __construct()
    {
        $this->score = $this->getScore();
        $this->created_at = new \DateTime();
    }

    /**
     * Return the comment unique id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param  string
     * @return null
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Sets the creation date
     * @param $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        if(!$createdAt instanceof \DateTime)
        {
            $createdAt = new \DateTime($createdAt);
        }
        $this->created_at = $createdAt;
    }

    public function __toString()
    {
        return 'Comment #'.$this->getId();
    }

    /**
     * Returns the depth of the comment.
     *
     * @return integer
     */
    public function getDepth()
    {
        return $this->depth;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(CommentInterface $parent)
    {
        $this->parent = $parent;

        if (!$parent->getId()) {
            throw new InvalidArgumentException('Parent comment must be persisted.');
        }

        $ancestors = $parent->getAncestors();
        $ancestors[] = $parent->getId();

        $this->setAncestors($ancestors);
    }

//    /**
//     * @return ThreadInterface
//     */
//    public function getThread()
//    {
//        return $this->thread;
//    }
//
//    /**
//     * @param ThreadInterface $thread
//     *
//     * @return void
//     */
//    public function setThread(ThreadInterface $thread)
//    {
//        $this->thread = $thread;
//    }


    /**
     * {@inheritDoc}
     */
    public function getModerationState()
    {
        return $this->moderationState;
    }

    /**
     * @param $state
     */
    public function setModerationState($state)
    {
        $this->previousModerationState = $this->moderationState;
        $this->moderationState = $state;
    }

    /**
     * @return int
     */
    public function getPreviousModerationState()
    {
        return $this->previousModerationState;
    }

    /**
     * @return bool
     */
    public function isVisible()
    {
        return $this->visibility;
    }

    /**
     * @param $visibility
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }

    /**
     * @return CommentUser
     */
    public function getCommentUser()
    {
        return $this->commentUser;
    }

    /**
     * @param CommentUser $commentUser
     */
    public function setCommentUser(CommentUser $commentUser)
    {
        $this->commentUser = $commentUser;
    }

    /**
     * @return string
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return int
     */
    public function getUpvotes()
    {
        return $this->upvotes;
    }

    /**
     * @param int $upvotes
     */
    public function setUpvotes($upvotes)
    {
        $this->upvotes = $upvotes;
        $this->score = $this->getScore();
    }

    /**
     * @return int
     */
    public function getDownvotes()
    {
        return $this->downvotes;
    }

    /**
     * @param int $downvotes
     */
    public function setDownvotes($downvotes)
    {
        $this->downvotes = $downvotes;
        $this->score = $this->getScore();
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return ($this->upvotes - $this->downvotes);
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}
