<?php

namespace La\CommentFrontBundle\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use La\CommentFrontBundle\Model\Comment;
use La\CommentFrontBundle\Model\Thread;
use La\CommentFrontBundle\Model\CommentUser;

abstract class ApiCommentManager implements CommentManagerInterface
{

    const LA_CFB_USER_COOKIE = "la_cfb_user";

    /**
     * @param $securityContext
     * @param $apiUrl
     * @param $userId
     * @param $userPw
     */
    public function __construct(TokenStorageInterface $securityContext, $apiUrl, $userId, $userPw)
    {
        $this->securityContext = $securityContext;
        $this->apiUrl = $apiUrl;
        $this->credentials = $userId . ":" . $userPw;
    }


    ///////////////////////////////// THESES FUNCTIONS MUST BE OVERRIDDEN //////////////////////////////

    /**
     * This function allows the application to generate a thread factory from a content identifier
     *
     * @param $contentId
     * @return mixed
     */
    abstract public function threadIdFactory($contentId);

    /**
     * This function allows the application to get a comment user from a request
     * The method body should call getLoggedUser() and getAnonymousUser() accordingly.
     *
     * @param $request
     * @return mixed
     */
    abstract public function getCommentUser(Request $request);

    /**
     * This function allows the application to get a website user from a comment user
     *
     * @param $commentUser
     * @return mixed
     */
    abstract public function getUserFromCommentUser(CommentUser $commentUser);


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $cookies
     * @param string $anonUsername
     * @return CommentUser
     */
    public function getAnonymousUser($cookies, $anonUsername = 'Anonyme')
    {
        return $this->getUser(0, $anonUsername, null, null, $cookies, false);
    }

    /**
     * @param $userId
     * @param $username
     * @param $email
     * @param $avatar
     * @param $cookies
     * @return CommentUser
     */
    public function getLoggedUser($userId, $username, $email, $avatar, $cookies)
    {
        return $this->getUser($userId, $username, $email, $avatar, $cookies, true);
    }

    /**
     * @param $userId
     * @param $username
     * @param $email
     * @param $avatar
     * @param $cookies
     * @param $logged
     * @return CommentUser
     */
    protected function getUser($userId, $username, $email, $avatar, $cookies, $logged)
    {
        $commentUser = new CommentUser();
        $commentUser->setCookie($cookies);
        $commentUser->setLogged($logged);
        $commentUser->setUsername($username);
        $commentUser->setEmail($email);
        $commentUser->setAvatar($avatar);
        $commentUser->setUserId($userId);
        return $commentUser;
    }

    /**
     * @param Request $request
     * @throws \InvalidArgumentException
     * @return Cookie $cookie;
     */
    public function getCommentUserCookie(Request $request)
    {

        $cookie = $request->cookies->get(static::LA_CFB_USER_COOKIE);

        if (is_null($cookie)) {
            $value = sha1($request->getClientIp() . sha1(time() . sha1(microtime())));
        } else {
            $value = $cookie;
        }

        $expires = new \DateTime();
        $expires->modify('+10years');

        $split = explode('.', $request->getHttpHost());
        $len = count($split);

        if ($len < 2) {
            throw new \InvalidArgumentException('Invalid Host');
        } else if ($len === 2) {
            $domain = $request->getHttpHost();
        } else {
            $domain = sprintf('%s.%s', $split[$len - 2], $split[$len - 1]);
        }

        $cookie = new Cookie(static::LA_CFB_USER_COOKIE, $value, $expires->getTimestamp(), '/', $domain, false, false);
        setcookie($cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(), $cookie->getDomain(), $cookie->isSecure(), $cookie->isHttpOnly());

        return $cookie;
    }

    /**
     * @param Request $request
     * @return mixed|string
     */
    public function getSerializedHeaders(Request $request)
    {
        $validHeaders = array();

        foreach ($request->server->all() as $key => $header) {
            if (substr($key, 0, 5) === 'HTTP_' || $key == 'REMOTE_ADDR' || $key == 'PHP_SELF') {
                $validHeaders[$key] = $header;
            }
        }
        return @serialize($validHeaders);
    }

    /**
     * @param $threadId
     * @param $permalink
     * @param $threadType
     * @return array
     */
    public function newThread($threadId, $permalink, $threadType = Thread::THREAD_TYPE)
    {
        $url = $this->apiUrl;
        $data = array(
            "la_comment_thread[id]" => $threadId,
            "la_comment_thread[permalink]" => $permalink,
            "la_comment_thread[type]" => $threadType
        );
        try {
            $cURLData = $this->cURL($url, 'POST', $data);
            $thread = $this->arrayThreadToObject($cURLData);
            if (!$thread instanceof Thread) {
                throw new \Exception('Internal Server Error');
            }
        } catch (\Exception $e) {
            // On error return debug empty thread to prevent errors
            return $this->getDebugThread();
        }
    }

    /**
     * @param $threadId
     * @return Thread|null
     */
    public function getThread($threadId)
    {
        $url = sprintf("%s/%s", $this->apiUrl, $threadId);
        try {
            $cURLData = $this->cURL($url);
            if (isset($cURLData['thread'])) {
                $thread = $this->arrayThreadToObject($cURLData['thread']);
                if (!$thread instanceof Thread) {
                    throw new \Exception('Internal Server Error');
                }
                return $thread;
            } else {
                throw new \Exception('Internal Server Error.');
            }
        } catch (\Exception $e) {
            // On error return debug empty thread to prevent errors
            return $this->getDebugThread();
        }
    }

    /**
     * @param $threadId
     * @param $permalink
     * @param $sorter
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getCommentsFromThread($threadId, $permalink, $sorter, $offset = null, $limit = null)
    {
        $url = sprintf("%s/%s/comments?permalink=%s&sorter=%s", $this->apiUrl, $threadId, $permalink, $sorter);

        if (!is_null($offset)) {
            $url .= sprintf('&offset=%d', $offset);
        }

        if (!is_null($limit)) {
            $url .= sprintf('&limit=%d', $limit);
        }

        try {
            $cURLData = $this->cURL($url);

            if (!isset($cURLData['comments']) || !isset($cURLData['thread'])) {
                throw new \Exception("Internal Server Error.");
            }

            $arrayComments = $this->extractCommentsFromData($cURLData['comments']);
            $thread = $this->arrayThreadToObject($cURLData['thread']);

            if (!$thread instanceof Thread) {
                throw new \Exception("Internal Server Error.");
            }

            $result = array(
                'comments' => $arrayComments,
                'thread' => $thread
            );
            return $result;

        } catch (\Exception $e) {
            // On error return debug empty thread to prevent errors
            return array(
                'comments' => array(),
                'thread' => $this->getDebugThread()
            );
        }
    }


    /**
     * @param $threadId
     * @param $commentId
     * @return array
     */
    public function getCommentFromThread($threadId, $commentId)
    {
        $url = sprintf("%s/%s/comments/%s", $this->apiUrl, $threadId, $commentId);

        try {
            $cURLData = $this->cURL($url);
            $comment = $this->arrayCommentToObject($cURLData['comment']);
            if (!$comment instanceof Comment) {
                throw new \Exception('Internal Server Error');
            }
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * @param $threadId
     * @param $body
     * @param $headers
     * @param null $parent
     * @param null $user
     * @param int $rating
     * @param null $title
     * @return array
     */
    public function newComment($threadId, $title = null, $body, $rating = 0, $headers, $parent = null, $user = null)
    {
        $url = sprintf("%s/%s/comments%s", $this->apiUrl, $threadId, is_null($parent) ? "" : "?parentId=" . $parent);
        $data = array(
            "la_comment_comment[title]" => $title,
            "la_comment_comment[body]" => $body,
            "la_comment_comment[rating]" => $rating,
            "la_comment_comment[headers]" => $headers
        );

        /** @var CommentUser $user */

        $data["la_comment_comment[cookie]"] = (string)$user->getCookie();
        $data["la_comment_comment[userId]"] = $user->getUserId();
        $data["la_comment_comment[username]"] = $user->getUsername();
        $data["la_comment_comment[email]"] = $user->getEmail();
        $data["la_comment_comment[avatar]"] = $user->getAvatar();

        try {
            $cURLData = $this->cURL($url, 'POST', $data);
            if (!is_array($cURLData)) {
                throw new \Exception('Internal Server Error');
            }

            $comments = $this->extractCommentsFromData([
                [
                    'comment' => $cURLData,
                    'children' => []
                ]
            ]);
            $thread = $this->getThread($threadId);
            if (!$thread instanceof Thread) {
                throw new \Exception('Internal Server Error');
            }

            return [
                'comments' => $comments,
                'thread' => $thread
            ];
        } catch (\Exception $e) {
            // On error return debug empty thread to prevent errors
            return array(
                'comments' => array(),
                'thread' => $this->getDebugThread()
            );
        }
    }

    /**
     * @param $threadId
     * @param $commentId
     * @param $body
     * @param $headers
     * @param $user
     * @param null $title
     * @return array
     */
    public function editComment($threadId, $commentId, $body, $headers, $user, $title = null)
    {
        $url = sprintf("%s/%s/comments/%s", $this->apiUrl, $threadId, $commentId);
        $data = array(
            "la_comment_comment[title]" => $title,
            "la_comment_comment[body]" => $body,
            "la_comment_comment[headers]" => $headers
        );
        try {
            $cURLData = $this->cURL($url, 'PUT', $data);

            if (!isset($cURLData['thread'])) {
                throw new \Exception("Internal Server Error.");
            }

//            $arrayComments = $this->extractCommentsFromData($cURLData['comments']);
            $comment = $this->arrayCommentToObject($cURLData);
            $thread = $this->arrayThreadToObject($cURLData['thread']);

            if (!$thread instanceof Thread || !$comment instanceof Comment) {
                throw new \Exception("Internal Server Error.");
            }

            $result = array(
                'comment' => $comment,
                'thread' => $thread
            );
            return $result;
        } catch (\Exception $e) {
            // On error return debug empty thread to prevent errors
            return array(
                'comments' => array(),
                'thread' => $this->getDebugThread()
            );
        }
    }

    /**
     * @param $threadId
     * @param $commentId
     * @param $userId
     * @param $vote
     * @return array
     */
    public function voteComment($threadId, $commentId, $userId, $vote)
    {
        if (is_null($userId)) {
            return array();
        }

        $url = sprintf("%s/%s/comments/%s/votes", $this->apiUrl, $threadId, $commentId);
        $data = array(
            "la_comment_vote[value]" => $vote,
            "la_comment_vote[user]" => $userId
        );

        try {
            $cURLData = $this->cURL($url, 'POST', $data);
            $comment = $this->arrayCommentToObject($cURLData);
            if (!$comment instanceof Comment) {
                throw new \Exception('Internal Server Error');
            }
            return $comment;
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * @param $threadId
     * @param $userId
     * @return array|mixed
     */
    public function getUserInfoFromThread($threadId, $userId)
    {
        $url = sprintf("%s/%s/users/%s", $this->apiUrl, $threadId, $userId);

        $cURLData = $this->cURL($url);
        try {
            return $cURLData;
        } catch (\Exception $e) {
            return array();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                            NOT INTERFACE                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $url
     * @param $method
     * @param null $data
     * @return mixed
     */
    protected function cURL($url, $method = 'GET', $data = null)
    {
        if ($method === 'POST') {
            $data = http_build_query($data);
        }

        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_USERPWD, $this->credentials);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method));

        if (!is_null($data)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        $return = curl_exec($curl);
        curl_close($curl);
        return is_null(json_decode($return, true)) ? $return : json_decode($return, true);
    }

    /**
     * @param $data
     * @return array
     */
    protected function arrayCommentToObject($data)
    {
        // A comment has properties from both itself and its user :
        $comment = new Comment();
        $reflectionComment = new \ReflectionClass($comment);

        $user = new CommentUser();
        $reflectionUser = new \ReflectionClass($user);

        foreach ($data as $property => $value) {
            // Comment : has properties
            if ($reflectionComment->hasProperty($property)) {
                $reflection = $reflectionComment;
                $object = $comment;

                if ($property == 'thread') {
                    $value = $this->arrayThreadToObject($value);
                }
            } else if ($reflectionUser->hasProperty($property)) {
                // User : has properties
                $reflection = $reflectionUser;
                $object = $user;
            } else {
                continue;
            }

            $reflectionProperty = $reflection->getProperty($property);
            $reflectionProperty->setAccessible(true);
            $reflectionProperty->setValue($object, $value);
        }

        $comment->setCommentUser($user);

        return $comment;
    }

    /**
     * @param $data
     * @return Thread
     */
    protected function arrayThreadToObject($data)
    {
        $thread = new Thread();
        if (!is_null($data) && !empty($data)) {
            $reflection = new \ReflectionClass($thread);
            foreach ($data as $property => $value) {
                if ($reflection->hasProperty($property)) {
                    $reflectionProperty = $reflection->getProperty($property);
                    $reflectionProperty->setAccessible(true);
                    $reflectionProperty->setValue($thread, $value);
                }
            }
        }
        return $thread;
    }

    /**
     * Recursif
     *
     * @param $data
     * @return array
     */
    protected function extractCommentsFromData($data)
    {
        $arrayComments = [];
        foreach ($data as $comment) {
            $arrayComments[] = [
                'comment' => $this->arrayCommentToObject($comment['comment']),
                'children' => $this->extractCommentsFromData($comment['children'])
            ];
        }
        return $arrayComments;
    }

    /**
     * @return Thread
     */
    protected function getDebugThread()
    {
        $thread = new Thread();
        $thread->setId('error');
        $thread->setEnabled(false);
        return $thread;
    }
}
