<?php

namespace La\CommentFrontBundle\Model;

use Symfony\Component\HttpFoundation\Request;

interface CommentManagerInterface
{

    /**
     * Set thread identifier for a a given content
     *
     * @param $contentId    |   Identifier of the content the new thread is about
     * @return mixed
     */
    public function threadIdFactory($contentId);

    /**
     * This function allows the application to get a comment user from a request
     *
     * @param $request
     * @return mixed
     */
    public function getCommentUser(Request $request);

    /**
     * This function get an anonymous user.
     *
     * @param $cookies
     * @param string $anonUsername
     * @return mixed
     */
    public function getAnonymousUser($cookies, $anonUsername = 'Anonyme');

    /**
     * @param $userId
     * @param $username
     * @param $email
     * @param $avatar
     * @param $cookies
     * @return CommentUser
     */
    public function getLoggedUser($userId, $username, $email, $avatar, $cookies);

    /**
     * This function allows the application to get a website user from a comment user
     *
     * @param $commentUser
     * @return mixed
     */
    public function getUserFromCommentUser(CommentUser $commentUser);
    /**
     * Get the cookie identifying current comment user from the request
     *
     * @param Request $request
     * @return mixed
     */
    public function getCommentUserCookie(Request$request);


    /**
     * Get serialized headers to be stored in db
     *
     * @param Request $request
     * @return mixed
     */
    public function getSerializedHeaders(Request $request);

    /**
     * Create a new comments thread
     *
     * @param $resourceId   |   Identifier of the resource the new thread is about
     * @param $permalink    |   Permalink of comments thread
     * @param $threadType   |   Thread type : class constants
     * @return mixed
     */
    public function newThread($resourceId, $permalink, $threadType = Thread::THREAD_TYPE);

    /**
     * Get information about a given thread
     *
     * @param $threadId     |   Comments thread identifier
     * @return mixed
     */
    public function getThread($threadId);

    /**
     * Get all the comments from a given thread
     *
     * @param $threadId     |   Comments thread identifier
     * @param $permalink    |   Permalink of comments thread
     * @param $sorter       |   Comments sorter
     * @param $offset
     * @param $limit
     * @return mixed
     */
    public function getCommentsFromThread($threadId, $permalink, $sorter, $offset = null, $limit = null);

    /**
     * Get a given comment from a given thread
     *
     * @param $threadId     |   Comments thread identifier
     * @param $commentId    |   Comment identifier
     * @return mixed
     */
    public function getCommentFromThread($threadId, $commentId);

    /**
     * Create a new comment in a given thread
     *
     * @param $threadId     |   Comments thread identifier
     * @param $title        |   Comment title (falcutative)
     * @param $body         |   Body of the new comment
     * @param $rating       |   Content rating in comment (falcutative)
     * @param $headers      |   HTTP headers of comment author
     * @param null $parent  |   Ancestor of the new comment. Can be null.
     * @param null $user    |   Comment author
     * @return mixed
     */
    public function newComment($threadId, $title, $body, $rating, $headers, $parent, $user);

    /**
     * Edit a given comment from a given thread
     *
     * @param $threadId     |   Comments thread identifier
     * @param $commentId    |   Comment identifier
     * @param $body         |   Edited body of the comment
     * @param $headers      |   HTTP headers of comment modifier
     * @param $user         |   User modifier ( Must match user author )
     * @return mixed
     */
    public function editComment($threadId, $commentId, $body, $headers, $user);

}



