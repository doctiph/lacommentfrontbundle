<?php

namespace La\CommentFrontBundle\Controller;

use La\CommentFrontBundle\Model\Thread;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DemoCommentsController extends Controller
{
    public function contentAction($id)
    {
        if($this->container->get('kernel')->getEnvironment() != 'dev')
        {
            return new Response('Dev only', 403);
        }

        if(in_array($id,array(1,2,3,4,5)))
        {
            $tpl = 'LaCommentFrontBundle:DemoComments:content_' . $id . '.html.twig';
        }else{
            $tpl = 'LaCommentFrontBundle:DemoComments:new.html.twig';
        }

        return $this->render(
            $this->get('templating')->exists($tpl) ? $tpl : 'LaCommentFrontBundle:DemoComments:content_error.html.twig',
            array(
                "thread_id" => $this->get('la_comment.front.comment_manager')->threadIdFactory($id),
                "resource_id" => $id,
                "type" => Thread::THREAD_TYPE,
                "login_url" => $this->container->getParameter('la_comment_front.login_url')
            )
        );
    }
}
