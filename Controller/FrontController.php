<?php

namespace La\CommentFrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use La\CommentFrontBundle\Model\Comment;
use La\CommentFrontBundle\Model\Thread;
use La\CommentFrontBundle\Model\CommentUser;

class FrontController extends Controller
{

    protected $comment_prefix = 'default';

    /**
     * Thread route, used for further routing
     */
    public function defaultAction(Request $request)
    {
        return new Response(null);
    }

    /**
     * Post new comment
     *
     * @param Request $request
     * @param $thread
     * @param null $parent
     * @return Response
     */
    public function newAction(Request $request, $thread, $parent = null)
    {

        if (!$request->isXmlHttpRequest()) {
            return new Response('XmlHttpRequests only.', 204);
        }
        $manager = $this->get('la_comment.front.comment_manager');
        $user = $manager->getCommentUser($request);
        $name = 'la_front_comment_post';
        if (!is_null($parent)) {
            $name .= '_' . $parent;
        }
        $form = $this->get('form.factory')->createNamedBuilder(
            $name,
            'la_front_comment_post',
            null,
            array('thread' => $manager->getThread($thread), 'parentId' => $parent)
        )->getForm();

        $form->submit($request);

        $allowAnonymous = $this->container->getParameter('la_comment_front.allow_anonymous');
        if (! $allowAnonymous && !$user->isLogged()) {
            return new Response('Anonymous comments are disabled.', 205);
        }

        if ($form->isValid()) {

            $data = $form->getData();
            // Comment
            $comment = $data['comment'];

            // Title
            if (isset($data['title'])) {
                $title = $data['title'];
            } else {
                $title = null;
            }

            // Rating
            if (isset($data['rating'])) {
                $rating = $data['rating'];
            } else {
                $rating = 0;
            }

            $result = $manager->newComment(
                $thread,
                $title,
                $comment,
                $rating,
                $manager->getSerializedHeaders($request),
                $parent,
                $user
            );

            try {
                if (isset($result['error'])) {
                    return new Response($result['error'], 206);
                }
                if (isset($result['comments']) && isset($result['comments'][0]) && $result['comments'][0]['comment'] instanceof Comment) {
                    /** @var Comment $comment */
                    $comment = $result['comments'][0]['comment'];
                    if ($comment->getModerationState() === $comment::STATE_PENDING && $comment->isVisible() == false) {
                        return new Response('pending', 202);
                    }
                }
            } catch (\Exception $e) {
                return new Response('invalid thread', 204);
            }
            return new Response('', 200);
        } else {
            $errors = $this->validAjax($form, $request);
            foreach ($errors as $error) {
                // last error
                $error = $error['message'];
            }
            if (!isset($error)) {
                $error = $form->getErrorsAsString();
            }
            return new Response($error, 206);
        }

    }

    /**
     * @param Request $request
     * @param $threadId
     * @param $commentId
     * @return Response
     */
    public function voteAction(Request $request, $threadId, $commentId)
    {
        if (!$request->isXmlHttpRequest()) {
            return new Response('XmlHttpRequests only.', 204);
        }

        $parameters = $request->get('la_comment_vote');
        $vote = $parameters['value'];

        if (!in_array($vote, array(1, -1))) {
            return new Response('Bad vote syntax.', 204);
        }

        $manager = $this->get('la_comment.front.comment_manager');
        /** @var CommentUser $user */
        $user = $manager->getCommentUser($request);

        if (!$user->isLogged()) {
            return new Response('Anonymous comments are disabled.', 205);
        }

        try {
            $comment = $manager->voteComment($threadId, $commentId, $user->getUserId(), $vote);
        } catch (\Exception $e) {
            return new Response('invalid thread', 204);
        }

        return $this->renderVotesAction($comment, $manager->getThread($threadId));
    }

    /**
     * Render sorted comments from XHR
     *
     * @param Request $request
     * @param $threadId
     * @param string $sorter
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    public function sortAction(Request $request, $threadId, $sorter = 'date_desc')
    {
        $manager = $this->get('la_comment.front.comment_manager');
        try {
            $data = $manager->getCommentsFromThread($threadId, null, $sorter);
        } catch (\Exception $e) {
            return null;
        }

        return $this->render($this->getTemplate('comments_enabled_thread'), array('data' => $data));
    }

    /**
     * @param Request $request
     * @param $threadId
     * @return Response
     */
    public function userInfoAction(Request $request, $threadId)
    {
        if (!$request->isXmlHttpRequest()) {
            return new Response('XmlHttpRequests only.', 204);
        }

        $manager = $this->get('la_comment.front.comment_manager');
        $userId = $manager->getCommentUser($request)->getUserId();
        if (is_null($userId)) {
            return new Response('null', 204);
        }
        $userInfo = $manager->getUserInfoFromThread($threadId, $userId);
        return new Response($userInfo, 200);
    }

    //////////////////////////////////////////// TWIG RENDER ACTIONS ///////////////////////////////////////////////////

    /**
     * @param Comment $comment
     * @param Thread $thread
     * @return Response
     */
    public function renderVotesAction(Comment $comment, Thread $thread)
    {

        if ($thread->getType() === $thread::REVIEW_TYPE) {
            $tpl = "reviews_review_vote";
        } else {
            $tpl = "comments_comment_vote";
        }
        return $this->render($this->getTemplate($tpl), array('comment' => $comment));
    }

    /**
     * Render rating for a given comment
     *
     * @param Comment $comment
     * @return Response
     */
    public function renderRatingAction(Comment $comment)
    {
        $tpl = 'reviews_review_rating';
        return $this->render($this->getTemplate($tpl), array('rating' => $comment->getRating()));
    }

    /**
     * Render a comment
     *
     * @param $comment
     * @param Thread $thread
     * @return Response
     */
    public function renderCommentAction($comment, Thread $thread)
    {
        if ($thread->getType() === $thread::REVIEW_TYPE) {
            $tpl = "reviews_review";
        } else {
            $tpl = "comments_comment";
        }
        return $this->render($this->getTemplate($tpl), array('comment' => $comment, 'thread' => $thread));
    }

    /**
     * Render form from twig
     *
     * @param $data
     * @return Response
     */
    public function renderFormAction($data)
    {
        return $this->formAction($data['thread']->getId());
    }

    /**
     * Render all comments from twig
     *
     * @param $data
     * @return Response
     */
    public function renderDisplayAction($data)
    {
        return $this->displayAction($data);
    }

    /**
     * Render sorters from Twig
     *
     * @param $data
     * @return Response
     */
    public function renderSorterAction($data)
    {
        return $this->sorterAction($data);
    }

    //////////////////////////////////////////// XHR RENDER ACTIONS ///////////////////////////////////////////////////

    /**
     * Render form from XHR
     *
     * @param $threadId
     * @param null $parentId
     * @return Response
     * @throws \Exception
     */
    public function formAction($threadId, $parentId = null)
    {
        $manager = $this->get('la_comment.front.comment_manager');
        $thread = $manager->getThread($threadId);

        if (is_null($thread)) {
            Throw new \Exception ('Thread is null');
        }

        // Cas numéro 1 , le thread est commentable ET enabled : On peut poster.
        if ($thread->isCommentable() && $thread->isEnabled()) {

            $name = 'la_front_comment_post';
            if (!is_null($parentId)) {
                $name .= '_' . $parentId;
            }

            $form = $this->get('form.factory')->createNamedBuilder($name, 'la_front_comment_post', null, array(
                'attr' => array('id' => $name, 'name' => $name),
                'action' => $this->generateUrl('la_comment_new_comment', array(
                        'thread' => $thread->getId(),
                        'parent' => $parentId,
                    )),
                'method' => 'POST',
                'thread' => $thread,
                'parentId' => $parentId
            ))->getForm();

//            $form->get('resourceId')->setData($resourceId);
            $form->get('threadId')->setData($thread->getId());

            /** @var Thread $thread */
            if ($thread->getType() === $thread::REVIEW_TYPE) {
                $tpl = 'form_commentable_reviews';
            } else {
                $tpl = 'form_commentable';
            }
            return $this->render(
                $this->getTemplate($tpl),
                array(
                    'form' => $form->createView(),
                    'thread_id' => $thread->getId(),
                    'parent_id' => $parentId
                )
            );

        } // Cas numéro 2, le thread n'est pas commentable MAIS enabled : Locked
        else if ($thread->isEnabled()) {
            return $this->render($this->getTemplate('form_locked'));
        } // Tous les autres cas :
        // - "Non enabled"
        else {
            return new Response('', 200);
        }
    }

    /**
     * Render all comments from XHR
     *
     * @param $data
     * @return Response
     */
    public function displayAction($data)
    {
        // Cas numéro 1 : Il y a des commentaires et le thread est enabled
        if (!empty($data['comments']) && $data['thread']->isEnabled()) {
            return $this->render(
                $this->getTemplate('comments_enabled_thread'),
                array('data' => $data)
            );
        } // Cas numéro 2 : Il n'y a pas de commentaire MAIS le thread est commentable et le thread est enabled
        else if ($data['thread']->isCommentable() && $data['thread']->isEnabled()) {
            return $this->render($this->getTemplate('comments_empty_thread'));
        } // Tous les autres cas, c'est à dire :
        // - "Vide, non-commentable et enabled"
        // - "Non enabled"
        else {
            return $this->render($this->getTemplate('comments_locked_thread'));
        }
    }

    /**Render sorters from XHR
     *
     * @param $data
     * @return Response
     */
    public function sorterAction($data)
    {
        if (count($data['comments']) > 1) {
            /** @var Thread $thread */
            $thread = $data['thread'];
            if ($thread->getType() === $thread::REVIEW_TYPE) {
                $tpl = "reviews_sort_links";
            } else {
                $tpl = "sort_links";
            }
            return $this->render($this->getTemplate($tpl), array('thread_id' => $data['thread']->getId()));
        } else {
            return $this->render($this->getTemplate('sort_empty_thread'), array('thread_id' => $data['thread']->getId()));

        }
    }

    /**
     * Render stats from XHR
     *
     * @param $threadId
     * @return Response
     */
    public function statsAction($threadId)
    {
        $manager = $this->get('la_comment.front.comment_manager');
        $thread = $manager->getThread($threadId);
        if ($thread->getType() === $thread::REVIEW_TYPE) {
            $tpl = "reviews_thread_stats";
        } else {
            $tpl = "thread_stats";
        }
        return $this->render($this->getTemplate($tpl), array('thread' => $thread));
    }

    //////////////////////////////////////////// <not> ACTIONS ///////////////////////////////////////////////////

    /**
     * @param $form
     * @param Request $request
     * @return array
     */
    public function validAjax($form, Request $request)
    {
        $errors = array();

        if (!$form->isValid()) {
            $errorList = $this->get('validator')->validate($form);

            foreach ($errorList as $error) {
                $propertyPath = $error->getPropertyPath();
                $invalidValue = $error->getInvalidValue();

                if (preg_match_all('#data\.(.*)$#', $propertyPath, $matches)) {
                    $propertyPath = str_replace($matches[0][0], 'children[' . $matches[1][0] . ']', $propertyPath);
                }

                if (is_array($invalidValue)) {
                    foreach ($invalidValue as $key => $val) {
                        $errors[$propertyPath . '.children[' . $key . '].data'] = array(
                            'message' => $error->getMessage(),
                            'invalidValue' => $val
                        );
                    }
                } else {
                    if (!preg_match('#.data$#', $propertyPath)) {
                        $propertyPath .= '.data';
                    }
                    $errors[$propertyPath] = array(
                        'message' => $error->getMessage(),
                        'invalidValue' => $error->getInvalidValue(),
                    );
                }
            }
        }
        return $errors;

    }

    /**
     * @param $name
     * @return mixed
     */
    protected function getTemplate($name)
    {
        return $this->container->getParameter(
            sprintf('%s.%s', $this->comment_prefix, $name)
        );
    }

}
